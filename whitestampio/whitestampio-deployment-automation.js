const express = require('express');
const childProcess = require('child_process');
const bodyParser = require('body-parser');
const githubUsername = 'hooannaeis';
const port = 8181;

var app = express();
app.use(bodyParser.json());

app.post("/webhooks/build", function (req, res) {
  console.log(req.body);
  const sender = req.body.actor.nickname;
  const branch = req.body.push.changes[0].new.name;

  if (branch.indexOf('master') > -1 && sender === githubUsername) {
    deploy(res);
  }
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

function deploy(res) {
  /**
   * @param {Object} res response to send back as an answer to the api-call
   * this function runs the whitestampio-build.sh script, which 
   * pulls from the master-branch of the connected repository
   * and then restarts the pm2-process for the app
   */
  childProcess.exec('cd && repositories/webhooks/whitestampio/whitestampio-build.sh', function(err) {
    if (err) {
      console.log(err);
      return res.sendStatus(500);
    } else {
      console.log('whitestamp app restartet');
      return res.sendStatus(200);
    }
  })
}
