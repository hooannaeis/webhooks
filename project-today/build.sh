# activate console logs
set -x

# go back to the home directory first
cd ~

# switch to the apps directory
cd repositories/project-today/

# pull from the master branch
# this step requires the credentials to the repo 
# to be stored locally on the machine 
git pull origin master

echo 'pulled successfully from master'

echo 'restarting server'

# restart the pm2 process which manages the app
pm2 restart app

echo 'server restarted successfully'

# deactivate console logs
set -x

