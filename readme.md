# Webhooks for build automation
This repository contains build scripts for different web-apps, which are used to automatically deploy changes that are made on successfull pushes to the master branch. 
These build-scripts require three steps to work:

1. Configure the webhook on the git-provider (gitHub, Bitbucket, etc) to send a POST-request to the server of the app notifying it about a successful push to the master branch of the repository
1. set up an additional express-server listening at a port of your choice for the POST-request that would originate from the git-provider. this server is specified in the appname-deployment-automation.js file in the project folder. 
1.  if using a load-balancer make sure it is routing the requests to the POST-endpoint properly to the express-server you created under 2. 


